export ZPLUG_HOME=/usr/local/opt/zplug
source $ZPLUG_HOME/init.zsh

if [ "$TMUX" = "" ]; then
	tmux -u new-session -A -s 0
fi
# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"
export PATH="$PATH:$HOME/bin"

# use vim as manpage viewer
export MANPAGER="/bin/sh -c \"unset PAGER; \
	col -b -x | \
	vim -l --noplugin -u NONE -R \
	--cmd 'so ~/.vim/pager.vimrc' -\""

ZSH_THEME="agnoster"
DEFAULT_USER=$USER
ZSH_TMUX_AUTOSTART=false
# Supress 'insecure directories' warning on MacOS
ZSH_DISABLE_COMPFIX=true
bindkey -v

# For vi mode to work properly
export RPS1="%{$reset_color%}"

export PATH=$PATH:~/.platformio/penv/bin
#if [[ $TERM == xterm ]]; then TERM=xterm-256color; fi

# Uncomment the following line to use case-sensitive completion.
	# CASE_SENSITIVE="true"

# Uncomment the following line to use hyphen-insensitive completion. Case
	# sensitive completion must be off. _ and - will be interchangeable.
#HYPHEN_INSENSITIVE="true"

# Uncomment the following line to disable bi-weekly auto-update checks.
# DISABLE_AUTO_UPDATE="true"

# Uncomment the following line to change how often to auto-update (in days).
# export UPDATE_ZSH_DAYS=13

# Uncomment the following line to disable colors in ls.
# DISABLE_LS_COLORS="true"

# Uncomment the following line to disable auto-setting terminal title.
# DISABLE_AUTO_TITLE="true"

# Uncomment the following line to enable command auto-correction.
# ENABLE_CORRECTION="true"

# Uncomment the following line to display red dots whilst waiting for completion.
	# COMPLETION_WAITING_DOTS="true"

# Uncomment the following line if you want to disable marking untracked files

# under VCS as dirty. This makes repository status check for large repositories
		# much, much faster.
		# DISABLE_UNTRACKED_FILES_DIRTY="true"

# Uncomment the following line if you want to change the command execution time
	# stamp shown in the history command output.
	# You can set one of the optional three formats:
	# "mm/dd/yyyy"|"dd.mm.yyyy"|"yyyy-mm-dd"
	# or set a custom format using the strftime function format specifications,
	# see 'man strftime' for details.
		# HIST_STAMPS="mm/dd/yyyy"

# Would you like to use another custom folder than $ZSH/custom?
# ZSH_CUSTOM=/path/to/new-custom-folder

# Which plugins would you like to load? (plugins can be found in ~/.oh-my-zsh/plugins/*)
# Custom plugins may be added to ~/.oh-my-zsh/custom/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
plugins=(
	last-working-dir
	git
	yarn
	pip
	copyfile
	osx
	redis-cli
	systemd
	cargo
	pylint
	pipenv
	npm
    yarn
	python
	tmux
	web-search
	wd
	fzf
	zsh-syntax-highlighting
	emoji
	zsh-autosuggestions
	copyfile
	dirhistory
	osx
	jsontools
	conda-zsh-completion
)

# ZPlug
zplug "bckim92/zsh-autoswitch-conda"

source $ZSH/oh-my-zsh.sh

# User configuration
autoload -U zmv

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
if [[ -n $SSH_CONNECTION ]]; then
	export EDITOR='nvim'
else
	export EDITOR='nvim'
fi

# Compilation flags
export ARCHFLAGS="-arch arm64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
alias zshconfig="nvim ~/.zshrc"
alias zshreload="source ~/.zshrc"
alias ohmyzsh="nvim ~/.oh-my-zsh"
alias vimconfig="nvim ~/.vim/vimrc"

#if uname == "Darwin"; then
	#alias op="open"
#elif grep -q Microsoft /proc/version; then
	#alias op="explorer.exe"
#else
	#alias op="xdg-open"
#fi

# Short remove recursive force
alias rmd="rm -rf"

# Mkdir and mv
alias mkm=""
# Better ls
alias ls=lsd
alias ll='lsd -l --blocks name,date,size'
alias lld='lsd -l --blocks name,date,size -t'
alias lls='lsd -l --blocks name,date,size -S'
alias lst='lsd --tree'

# Update all pip packages
alias pipup="pip list --outdated --format=freeze | grep -v '^\-e' | cut -d = -f 1  | xargs -n1 pip3 install -U --user"

# IJulia
alias ijulia="jupyter console --kernel=julia-1.5"
# alias ipython="jupyter console"
#Pluto
alias pluto="julia ~/bin/start-pluto.jl"

# Config git repository
alias config='/usr/bin/git --git-dir=$HOME/.sync-config/ --work-tree=$HOME'
alias pkglist='yay -Qqe > $HOME/Utils/pkglist.txt'
alias pkgrestore='yay -S --needed $(/usr/bin/cat $HOME/Utils/pkglist.txt)'

# xclip
alias setclip="xclip -selection c"
alias getclip="xclip -selection c -o"

#Source file for additional sources/settings if available e.g. for ROS
	if [[ -r ~/.additionals ]]; then
		source ~/.additionals
	fi

compdef config='git'

# Settings for vim readline
function zle-line-init zle-keymap-select() {
	case "${KEYMAP}" in
		"vicmd")
			echo -ne '\033[2 q' ;;
		"main"|"viins"|"")
			echo -ne '\033[6 q' ;;
	esac

	zle reset-prompt
	zle -R
}

# Ensure that the prompt is redrawn when the terminal size changes.
TRAPWINCH() {
  zle &&  zle -R
}

zle -N zle-keymap-select
zle -N edit-command-line

# Better searching in command mode
bindkey -M vicmd '?' history-incremental-search-backward
bindkey -M vicmd '/' history-incremental-search-forward

# Make escape key more reactive
KEYTIMEOUT=18 # * 10 ms

# Beginning search with arrow keys
bindkey "^P" up-line-or-beginning-search
bindkey "^N" down-line-or-beginning-search
bindkey "^[[A" up-line-or-beginning-search
bindkey "^[[B" down-line-or-beginning-search
bindkey "^[OA" up-line-or-beginning-search
bindkey "^[OB" down-line-or-beginning-search
bindkey -M vicmd "k" up-line-or-beginning-search
bindkey -M vicmd "j" down-line-or-beginning-search
bindkey kj vi-cmd-mode
bindkey "\e" vi-cmd-mode

function change-cursor-on-accept() {
	echo -ne '\033[2 q'
	zle accept-line
}
zle -N change-cursor-on-accept change-cursor-on-accept
bindkey -M viins "^M" change-cursor-on-accept

function vi_mode_prompt_info() {
  echo "${${KEYMAP/vicmd/$MODE_INDICATOR}/(main|viins)/}"
}

function mkmv () {
	mkdir $1 && mv $1
}
# define right prompt, if it wasn't defined by a theme
if [[ "$RPS1" == "" && "$RPROMPT" == "" ]]; then
  RPS1='$(vi_mode_prompt_info)'
fi
export PATH="$HOME/.cargo/bin:$PATH"

export PATH="/usr/local/opt/llvm/bin:$PATH"
export LDFLAGS="-L/usr/local/opt/llvm/lib"
export CPPFLAGS="-I/usr/local/opt/llvm/include"
export PATH="/usr/local/opt/openssl@1.1/bin:$PATH"
export SUMO_HOME="/usr/local/opt/sumo/share/sumo"
export PATH="/usr/local/sbin:$PATH"

# homebrew readline
export LDFLAGS="-L/usr/local/opt/readline/lib"
export CPPFLAGS="-I/usr/local/opt/readline/include"

# homebrew sqlite
export PATH="/usr/local/opt/sqlite/bin:$PATH"

export LDFLAGS="-L/usr/local/opt/sqlite/lib"
export CPPFLAGS="-I/usr/local/opt/sqlite/include"

export PATH="$HOME/.local/bin:$PATH"
'e'xport CMAKE_PREFIX_PATH=/usr/local/Cellar/qt5/5.15.1/  
export PATH="/usr/local/opt/openjdk/bin:$PATH"
if [ "$(uname 2> /dev/null)" != "Linux" ]; then
	# For coc-xml
	export JAVA_HOME="$(/usr/libexec/java_home)"
fi

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
[ -f "/Users/manuel/.ghcup/env" ] && source "/Users/manuel/.ghcup/env" # ghcup-env
#compdef pio
_pio() {
  eval $(env COMMANDLINE="${words[1,$CURRENT]}" _PIO_COMPLETE=complete-zsh  pio)
}
if [[ "$(basename -- ${(%):-%x})" != "_pio" ]]; then
  compdef _pio pio
fi

alias pip="noglob pip3"


# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init' !!
__conda_setup="$('/Users/manuel/bin/miniforge3/bin/conda' 'shell.zsh' 'hook' 2> /dev/null)"
if [ $? -eq 0 ]; then
    eval "$__conda_setup"
else
    if [ -f "/Users/manuel/bin/miniforge3/etc/profile.d/conda.sh" ]; then
        . "/Users/manuel/bin/miniforge3/etc/profile.d/conda.sh"
    else
        export PATH="/Users/manuel/bin/miniforge3/bin:$PATH"
    fi
fi
unset __conda_setup
# <<< conda initialize <<<

