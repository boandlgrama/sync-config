" General {{
" Disable vi compatibility mode
set nocompatible
" Make mouse available in tmux
set mouse=a
" Line numbering
set number
set relativenumber
nmap <Leader>tl :set relativenumber!<CR>

" Move over visual lines
nnoremap j  gj
nnoremap k  gk

" let g:python_host_prog = '/Library/Frameworks/Python.framework/Versions/2.7/bin/python2'
let g:loaded_python_provider = 0
let g:python3_host_prog = '/usr/local/bin/python3'

" Search
set hlsearch
set incsearch
set ignorecase
set smartcase
" Use system clipboard
set clipboard=unnamed
" Visual autocomplete for command menu
set wildmenu
" Redraw only when needed
set lazyredraw
" Allow changing buffers without saving
set hidden
" Case insensitive file and directory completion
set wildignorecase

" Fix the escape sequence for curly underline, so it shows up again
exec "set t_Cs=\e[4:3m"
exec "set t_Ce=\e[4:0m"
set termguicolors

"}}}

" Key Mappings {{
inoremap kj <esc>
let mapleader = "ö"
let maplocalleader = 'ö'
nnoremap ß $
nnoremap ü ^

nnoremap <silent><leader># :noh<CR>
" smooth-scroll
noremap <silent> <c-u> :call smooth_scroll#up(&scroll, 10, 1)<CR>
noremap <silent> <c-d> :call smooth_scroll#down(&scroll, 10, 1)<CR>
noremap <silent> <c-b> :call smooth_scroll#up(&scroll*2, 10, 4)<CR>
noremap <silent> <c-f> :call smooth_scroll#down(&scroll*2, 10, 4)<CR>
" Merge view keybindings
" Choose remote change
nnoremap <C-m>r :diffget RE<CR>
" Choose common base
nnoremap <C-m>b :diffget BA<CR>
" Choose local change
nnoremap <C-m>l :diffget LO<CR>
" Jump to previous difference
nnoremap <C-m>p [c
" Jump to next difference
nnoremap <C-m>n ]c

" make it easier to use buffers
nnoremap gb :silent! bn<CR>
nnoremap gB :silent! bp<CR>

nnoremap <M-l> :silent! bn<CR>
nnoremap <M-h> :silent! bp<CR>

set runtimepath^=~/.vim/bundle/bbye
nnoremap <M-d> :silent! Bdelete<CR>
nnoremap <leader><M-d> :silent! Bdelete!<CR>

" NerdTree
" nnoremap <silent>ü :NERDTreeToggle<CR>

" Netrw
 function! ToggleVExplorer()
  if exists("t:expl_buf_num")
      let expl_win_num = bufwinnr(t:expl_buf_num)
      if expl_win_num != -1
          let cur_win_nr = winnr()
          exec expl_win_num . 'wincmd w'
          close
          exec cur_win_nr . 'wincmd w'
          unlet t:expl_buf_num
      else
          unlet t:expl_buf_num
      endif
  else
      exec '1wincmd w'
      Vexplore
      let t:expl_buf_num = bufnr("%")
  endif
endfunction
nnoremap <silent>ü :call ToggleVExplorer()<CR>
" Tree view option
let g:netrw_liststyle=3
" Make the directory indicators continuous
let s:treedepthstring="│ "
let g:netrw_banner = 0
let g:netrw_menu = 0
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 30
" nnoremap <silent>ü <Plug>VinegarVerticalSplitUp

" Easy align
xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

" fzf
" set rtp+=/usr/local/bin/fzf
nnoremap <space>f :FZF<CR>
nnoremap <space>h :Helptags<CR>
nnoremap <space>c :Commands<CR>
nnoremap <space>g :Commits<CR>
nnoremap <space>s :Snippets<CR>
nnoremap <space>y :History:<CR>
nnoremap <space>b :Buffers<CR>
nmap <leader><tab> <plug>(fzf-maps-n)
xmap <leader><tab> <plug>(fzf-maps-x)
omap <leader><tab> <plug>(fzf-maps-o)
" Insert mode completion
imap <c-x><c-k> <plug>(fzf-complete-word)
imap <c-x><c-f> <plug>(fzf-complete-path)
imap <c-x><c-l> <plug>(fzf-complete-line)
" Path completion with custom source command
inoremap <expr> <c-x><c-f> fzf#vim#complete#path('fd')
inoremap <expr> <c-x><c-f> fzf#vim#complete#path('rg --files')
" Word completion with custom spec with popup layout option
inoremap <expr> <c-x><c-k> fzf#vim#complete#word({'window': { 'width': 0.2, 'height': 0.9, 'xoffset': 1 }})
" This is the default option:
"   - Preview window on the right with 50% width
"   - CTRL-/ will toggle preview window.
" - Note that this array is passed as arguments to fzf#vim#with_preview function.
" - To learn more about preview window options, see `--preview-window` section of `man fzf`.
let g:fzf_preview_window = ['right:50%', 'ctrl-ö']

" Vim-test
nmap <silent> t<C-n> :TestNearest<CR>
nmap <silent> t<C-f> :TestFile<CR>
nmap <silent> t<C-s> :TestSuite<CR>
nmap <silent> t<C-l> :TestLast<CR>
nmap <silent> t<C-g> :TestVisit<CR>

" Quickfix
nmap <silent> <C-c>n :cn<CR>
nmap <silent> <C-c>p :cp<CR>
nmap <silent> <C-c>q :ccl<CR>
"}}}

" Indent {{
set shiftwidth=2
set tabstop=4
set softtabstop=4
"Allow automatic indentation
set autoindent
" Expand TABS to spaces
set expandtab
filetype plugin indent on
" set textwidth=80
"}}}

" Appearance {{
" Colorscheme
let g:gruvbox_italic = '1'
" The regular split character looks bad
set fillchars+=vert:│
" Indent line plugin
let g:indentLine_char = '│'
set background=dark
syntax on

" always show signcolumns for gitgutter
set signcolumn=yes
let g:gitgutter_override_sign_column_highlight = 0

" Airline
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline_theme='gruvbox'
let g:airline#extensions#vimtex#enabled = 1
let g:airline#extensions#coc#enabled = 1

"}}}

"Completion {{
" Omni completion
" set complete+=.,w,b,u,t,i,kspell
" set completefunc=syntaxcomplete#Complete
" set completeopt=longest,menuone,preview
"}}}

" COC{{
let g:coc_global_extensions = [
        \'coc-actions',
        \'coc-clangd',
        \'coc-cmake',
        \'coc-css',
        \'coc-docker',
        \'coc-emmet',
        \'coc-eslint',
        \'coc-explorer',
        \'coc-flow',
        \'coc-git',
        \'coc-highlight',
        \'coc-html',
        \'coc-java',
        \'coc-json',
        \'coc-lists',
        \'coc-markdownlint',
        \'coc-marketplace',
        \'coc-pairs',
        \'coc-phpls',
        \'coc-prettier',
        \'coc-pyright',
        \'coc-pydocstring',
        \'coc-rls',
        \'coc-rust-analyzer',
        \'coc-sh',
        \'coc-smartf',
        \'coc-snippets',
        \'coc-sql',
        \'coc-svg',
        \'coc-tsserver',
        \'coc-xml',
        \'coc-yaml',
        \'coc-yank',
        \'coc-cmake',
        \'coc-cssmodules',
        \'coc-fzf-preview',
        \'coc-html',
        \'coc-rome',
        \'coc-texlab',
        \'coc-julia',
        \'coc-vimtex',
        \'coc-r-lsp',
        \'coc-toml',
        \'coc-spell-checker',
        \'coc-cspell-dicts',
        \'coc-stylelint',
        \'coc-stylelintplus',
        \]

nnoremap <silent> <localleader>h :call CocActionAsync("doHover")<CR>
nmap <silent> <localleader>d <Plug>(coc-definition)
nmap <silent> <localleader>D <Plug>(coc-declaration)
nmap <silent> <localleader>t <Plug>(coc-type-definition)
nmap <silent> <localleader>r <Plug>(coc-rename)
nmap <silent> <localleader>R <Plug>(coc-references)
nmap <silent> <localleader>f <Plug>(coc-format)
vmap <silent> <localleader>f <Plug>(coc-format-selected)
nmap <silent> <localleader>a <Plug>(coc-codeaction-selected)
vmap <silent> <localleader>a <Plug>(coc-codeaction-selected)
nmap <silent> <localleader>c <Plug>(coc-fix-current)
" Apply AutoFix to problem on the current line.
nmap <leader>qf  <Plug>(coc-fix-current)

" Use <TAB> for select selections ranges, needs server support, like: coc-tsserver, coc-python
" nmap <silent> <TAB> <Plug>(coc-range-select)
" xmap <silent> <TAB> <Plug>(coc-range-select)

" Use K to show documentation in preview window
nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  else
    call CocAction('doHover')
  endif
endfunction


" Some servers have issues with backup files, see #649
set nobackup
set nowritebackup

" Better display for messages
set cmdheight=1

" You will have bad experience for diagnostic messages when it's default 4000.
set updatetime=300

" don't give |ins-completion-menu| messages.
set shortmess+=c

inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>":
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1]  =~# '\s'
endfunction

inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"
" Use <c-space> to trigger completion.
inoremap <silent><expr> <c-space> coc#refresh()

" For indentation after CR with the coc-pairs plugin
inoremap <silent><expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"
autocmd FileType rust let b:coc_pairs_disabled = ["'"]
" Highlight symbol under cursor on CursorHold
autocmd CursorHold * silent call CocActionAsync('highlight')

" Remap <C-f> and <C-b> for scroll float windows/popups.
if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

" Coc-snippets
let g:coc_snippet_next = '<M-k>'
let g:coc_snippet_prev = '<M-j>'
" Using CocList
" Show all diagnostics
nnoremap <silent> <space>a  :<C-u>CocList diagnostics<cr>
" Manage extensions
nnoremap <silent> <space>e  :<C-u>CocList extensions<cr>
" Show commands
nnoremap <silent> <space>c  :<C-u>CocList commands<cr>
" Find symbol of current document
nnoremap <silent> <space>o  :<C-u>CocList outline<cr>
" Search workspace symbols
nnoremap <silent> <space>s  :<C-u>CocList -I symbols<cr>
" Do default action for next item.
nnoremap <silent> <space>j  :<C-u>CocNext<CR>
" Do default action for previous item.
nnoremap <silent> <space>k  :<C-u>CocPrev<CR>
" Resume latest coc list
nnoremap <silent> <space>p  :<C-u>CocListResume<CR>

nmap <leader>sa :<C-u>CocCommand cSpell.addWordToUserDictionary<CR><CR>
" Remap keys for applying codeAction to the current buffer.

" Add status line support, for integration with other plugin, checkout `:h coc-status`
set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

set noshowmode
nmap f <Plug>(coc-smartf-forward)
nmap F <Plug>(coc-smartf-backward)
nmap ; <Plug>(coc-smartf-repeat)
nmap , <Plug>(coc-smartf-repeat-opposite)

augroup Smartf
  " autocmd User SmartfEnter :hi Conceal ctermfg=220 guifg=#6638F0
  autocmd User SmartfEnter :hi Conceal ctermfg=220 guifg=#58ed5f
  autocmd User SmartfLeave :hi Conceal ctermfg=239 guifg=#504945
augroup end

" hi CocUnderline gui=undercurl term=undercurl
" hi CocErrorHighlight ctermfg=red  guifg=#c4384b gui=undercurl term=undercurl
hi CocWarningHighlight ctermfg=red guifg=#c4ab39 gui=undercurl term=undercurl
hi CocHintHighlight ctermfg=green guifg=#c4ab39 gui=undercurl term=undercurl
hi CocInfoHighlight ctermfg=yellow guifg=#c4ab39 gui=undercurl term=undercurl

"}}}

" ALE {{
nnoremap <silent> <leader>F :ALEFix<CR>
let g:ale_sign_column_always = 1
let g:airline#extensions#ale#enabled = 1
let g:airline#extensions#ale#warning_symbol=" "
let g:airline#extensions#ale#error_symbol=" "
let g:airline#extensions#ale#show_line_numbers=1
let g:airline#extensions#ale#open_lnum_symbol=" "
let g:airline#extensions#ale#close_lnum_symbol=""
let g:ale_sign_error=" "
let g:ale_sign_warning=" "
let g:ale_echo_msg_error_str=" "
let g:ale_echo_msg_warning_str=" "
let g:ale_echo_msg_format='%severity% %s'
"let g:ale_set_balloons = 1
let g:ale_linters = {
      \ 'python': ['pyls', 'pylama', 'vulture']
      \ }
let g:ale_fixers = {
      \ '*': ['remove_trailing_lines', 'trim_whitespace'],
      \ 'python': ['add_blank_lines_for_python_control_statements', 'black', 'isort'],
      \ 'markdown': ['prettier', 'textlint'],
      \ 'php': ['prettier'],
      \ 'javascript': ['prettier', 'eslint'],
      \}
let b:ale_javascript_prettier_options = '--prose-wrap always'

highlight clear ALEError
highlight ALEError cterm=underline gui=underline ctermfg=Red guifg=#ff0000
highlight clear ALEWarning
highlight ALEWarn cterm=underline gui=underline guifg=#ff8800 ctermfg=Yellow
highlight clear SpellBad
highlight SpellBad cterm=undercurl gui=underline guifg=#ff8800 ctermfg=Yellow
" }}}

" Functionalities {{
" Closetags
let g:closetag_filetypes = 'html,xml,launch,urdf'

"More natureal split opening
set splitbelow
set splitright

" Enable code folding
set foldmethod=indent
set foldlevel=99
set modelines=1

" Remember cursor position
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

let g:echodoc#enable_at_startup = 1

" show last search results on the quickfix window at windows bottom
nnoremap <silent><leader>ss :vimgrep /<C-r>\// % <CR> :botright copen <CR>

" Increment letters
set nrformats+=alpha

" Tagbar
let g:tagbar_autoclose = 1
noremap <silent> ä : Tagbar<CR>

" Nerdcommenter
let g:NERDSpaceDelims = 1

" Diff with saved file
function! s:DiffWithSaved()
  let filetype=&ft
  diffthis
  vnew | r # | normal! 1Gdd
  diffthis
  exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
endfunction
com! DiffSaved call s:DiffWithSaved()

" Show whitespace characters
set listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:␣
command Whitespace set list!
nnoremap <leader>w :Whitespace<CR>

" }}}

" File type specific {{

let g:python_highlight_space_errors = 0

" ROS
" .launch files syntax higlighting
au BufRead,BufNewFile *.launch setfiletype xml "roslaunch

" Vimtex
let g:vimtex_toc_config= {
      \'split_pos'  :'vert rightbelow',
      \'fold_enable' : '0',
      \'layer_status' : { 'content':1,
      \'label':0,
      \'todo':1,
      \'include':0},
      \'split_width' : 50,
      \'tocdepth' : 3,
      \'show_numbers' : 0,
      \}
let g:vimtex_format_enabled = 1
" let g:vimtex_view_general_viewer = 'open -a Skim'
let g:vimtex_view_method = 'skim'
let g:vimtex_view_general_options = '--unique file:@pdf\#src:@line@tex'
let g:vimtex_view_general_options_latexmk = '--unique'
let g:tex_flavor = 'latex'
let g:vimtex_compiler_progname = 'nvr'
" let g:vimtex_compiler_callback_hooks = ['SetServerName']
let g:vimtex_quickfix_ignore_filters = [
  \'Underfull \\hbox (badness [0-9]*) in ',
  \'Overfull \\hbox ([0-9]*.[0-9]*pt too wide) in ',
  \]
let g:vimtex_compiler_latexmk = {
    \ 'options' : [
    \   '-pdf',
    \   '-shell-escape',
    \   '-verbose',
    \   '-file-line-error',
    \   '-synctex=1',
    \   '-interaction=nonstopmode',
    \ ],
    \}
" function! SetServerName(status)
  " call system('echo ' . v:servername . ' > /Users/manuel/tmp/curserver')
" endfunction

" Vim-markdown
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_conceal = 0
let g:vim_markdown_conceal_code_blocks = 0
let g:vim_markdown_new_list_item_indent = 2

" Bracey.vim
" let g:bracey_browser_command = 'safaridriver'
let g:bracey_browser_command = 'open -a "Google Chrome"'

" Julia
let g:latex_to_unicode_tab = 0
let g:JuliaFormatter_options = {'margin' : 81}
let g:JuliaFormatter_always_launch_server=1

let g:slime_target = "tmux"
let g:slime_default_config = {"socket_name": "default", "target_pane": "1"}
let g:slime_dont_ask_default = 1

let g:julia_cell_delimit_cells_by = 'tags'

nmap <leader>F :JuliaFormatterFormat<CR>
vmap <leader>F :JuliaFormatterFormat<CR>

" map <Leader>jr to run entire file
nnoremap <Leader>jr :JuliaCellRun<CR>
" map <Leader>jc to execute the current cell
nnoremap <Leader>jc :JuliaCellExecuteCell<CR>
" map <Leader>jC to execute the current cell and jump to the next cell
nnoremap <Leader>jC :JuliaCellExecuteCellJump<CR>
" map <Leader>jl to clear Julia screen
nnoremap <Leader>jl :JuliaCellClear<CR>
" map <Leader>jp and <Leader>jn to jump to the previous and next cell header
nnoremap <Leader>jp :JuliaCellPrevCell<CR>
nnoremap <Leader>jn :JuliaCellNextCell<CR>
" map <Leader>je to execute the current line or current selection
nmap <Leader>je <Plug>SlimeLineSend
xmap <Leader>je <Plug>SlimeRegionSend

" Matlab
let g:matlab_server_launcher = 'tmux' "launch the server in a tmux split
let g:matlab_server_split = 'horizontal' "launch the server in a horizontal split

" Vim markdown preview
" let vim_markdown_preview_hotkey='<C-m>'
" let vim_markdown_preview_toggle=1
" let vim_markdown_preview_temp_file=1
" " let vim_markdown_preview_pandoc=1
" let vim_markdown_preview_github=1

" let g:md_pdf_viewer="open -a Skim"
" let g:md_args = "--template eisvogel"
let g:mkdp_auto_close = 0
nmap <C-p> <Plug>MarkdownPreview

"}}}

" VIM only {{
if !has('nvim')
  set ttymouse=xterm2
  let &t_SI = "\033[6 q"
  let &t_SR = "\033[4 q"
  let &t_EI = "\033[2 q"

  " modespecific cursor
  if has("autocmd")
    au VimEnter,InsertLeave * silent execute '!echo -ne "\e[1 q"' | redraw!
    au InsertEnter,InsertChange *
          \ if v:insertmode == 'i' |
          \   silent execute '!echo -ne "\e[5 q"' | redraw! |
          \ elseif v:insertmode == 'r' |
          \   silent execute '!echo -ne "\e[3 q"' | redraw! |
          \ endif
    au VimLeave * silent execute '!echo -ne "\e[ q"' | redraw!
  endif
endif
" }}}
function! SyntaxGroup()                                                            
    let l:s = synID(line('.'), col('.'), 1)                                       
    echo synIDattr(l:s, 'name') . ' -> ' . synIDattr(synIDtrans(l:s), 'name')
endfun

" Plugins {{
" let g:polyglot_disabled = ['python', 'markdown']

call plug#begin('~/.vim/plugged')

" Appearance
Plug 'vim-airline/vim-airline'
Plug 'morhetz/gruvbox'
Plug 'ryanoasis/vim-devicons'
Plug 'kyazdani42/nvim-web-devicons'
Plug 'terryma/vim-smooth-scroll'
" Plug 'Yggdroot/indentLine'
Plug 'airblade/vim-gitgutter'

" Completion / Linting
Plug 'w0rp/ale'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'honza/vim-snippets'

" Filetype
Plug 'sheerun/vim-polyglot'
Plug 'lervag/vimtex'
Plug 'mattn/emmet-vim'
" Plug 'JamshedVesuna/vim-markdown-preview'
Plug 'tyru/open-browser.vim'
Plug 'weirongxu/plantuml-previewer.vim'
" Plug 'vim-pandoc/vim-pandoc-syntax'
" Plug 'vim-pandoc/vim-pandoc'
" Plug 'vimwiki/vimwiki'
" Plug 'conornewton/vim-pandoc-markdown-preview'
Plug 'turbio/bracey.vim'
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }
Plug 'JuliaEditorSupport/julia-vim'
Plug 'kdheepak/JuliaFormatter.vim'
Plug 'mroavi/vim-julia-cell', { 'for': 'julia' }
function! DoRemote(arg)
  UpdateRemotePlugins
endfunction

Plug 'daeyun/vim-matlab', { 'do': function('DoRemote') }

" Formating
Plug 'junegunn/vim-easy-align'
Plug 'tpope/vim-surround'
Plug 'alvan/vim-closetag'
Plug 'scrooloose/nerdcommenter'
Plug 'sbdchd/neoformat'
Plug 'tmhedberg/SimpylFold'

" Extensions
Plug 'majutsushi/tagbar'
" Plug 'scrooloose/nerdtree'
Plug 'tpope/vim-vinegar'

" Utils
Plug 'jremmen/vim-ripgrep'
Plug 'christoomey/vim-tmux-navigator'
Plug 'lambdalisue/suda.vim'
Plug '/usr/local/opt/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf'
Plug 'junegunn/fzf.vim'
Plug 'janko/vim-test'
Plug 'machakann/vim-highlightedyank'
Plug 'jpalardy/vim-slime'
Plug 'tpope/vim-repeat'
Plug 'sindrets/diffview.nvim'
Plug 'sillybun/vim-repl'

" Plug 'Shougo/echodoc.vim'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-speeddating'
call plug#end()
"}}}
colorscheme gruvbox
" vim:foldmethod=marker:foldlevel=0
